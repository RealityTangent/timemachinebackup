﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GPPR
{

    public class MouseLook : MonoBehaviour
    {
        //mouse sensitivity
        public float mouseSensitvity = 100f;
        //player refrence
        public Transform playerBody;
        //rotation refrence 
        float xRotation = 0f;

        //locks cursor for 360 rotation and no borders for mouse
        void Start()
        {
            Cursor.lockState = CursorLockMode.Locked;
        }

        void Update()
        {
            //sets sensitivity and rotation speed by time for X and Y
            float mouseX = Input.GetAxis("Mouse X") * mouseSensitvity * Time.deltaTime;
            float mouseY = Input.GetAxis("Mouse Y") * mouseSensitvity * Time.deltaTime;

            xRotation -= mouseY;
            xRotation = Mathf.Clamp(xRotation, -90f, 90f);

            transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
            playerBody.Rotate(Vector3.up * mouseX);
        }
    }
}
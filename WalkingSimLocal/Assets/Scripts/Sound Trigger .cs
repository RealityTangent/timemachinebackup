using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GPPR
{

    public class SoundTrigger : MonoBehaviour
    {
        //sound clip refrence
        public AudioClip soundToPlay;
        //volume refrence
        public float volume;
        //root file refrence 
        AudioSource audioRoot;
        //checks if audio has been played
        public bool alreadyPlayed = false;

        void Start()
        {
            //gets the audio from the source 
            audioRoot = GetComponent<AudioSource>();

        }

        //checks if the audio has already been played, if not plays according to the sound and which audio, then sets Played to true 
        void OnTriggerEnter(Collider other)
        {

            if (!alreadyPlayed)
            {
                audioRoot.PlayOneShot(soundToPlay, volume);
                alreadyPlayed = true;
            }

        }


    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GPPR
{

    public class PlayerMovementScript : MonoBehaviour
    {
        public CharacterController controller;

        public float speed;
        public float walkSpeed = 5f;
        public float sprintSpeed = 10f;
        public float gravity = -9.81f;
        public float jumpHeight = 3;

        //adds a public entry in the inspector where you can reference the correct game object
        public Transform groundCheck;
        //sets the radius of the ground sphere
        public float groundDistance = 0.4f;

        //uses the layer logic in unity. This is later used to determine if what the player is interacting with is the ground or not
        public LayerMask groundMask;

        //Creates a vactor of 3 for determining velocity. i.e (x axis, z axis, y axis)
        Vector3 velocity;
        bool isGrounded;

        void Update()
        {
            //Checks if the player is on the ground using an invisible sphere attached to a game object
            //therefore if this object collides with something with the ground mask, isGrounded gets set to true
            isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

            if (isGrounded && velocity.y < 0)
            {
                //sets the velocity on "y" to -2, not zero. This is to make sure the player always has some downward velocity and avoids issues
                velocity.y = -2f;
            }

            float x = Input.GetAxis("Horizontal");
            float z = Input.GetAxis("Vertical");

            if (Input.GetKey(KeyCode.LeftShift))
            {
                speed = sprintSpeed;
            }
            else
            {
                speed = walkSpeed;
            }

            Vector3 move = transform.right * x + transform.forward * z;

            controller.Move(move * speed * Time.deltaTime);

            if (Input.GetButtonDown("Jump") && isGrounded)
            {
                velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
            }

            //the velocity of Y is determined by taking the current velocity plus gravity and mulitplying it by time squared 
            //this code makes the player fall
            velocity.y += gravity * Time.deltaTime;

            controller.Move(velocity * Time.deltaTime);

        }
    }
}
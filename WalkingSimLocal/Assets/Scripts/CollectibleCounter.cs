using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace GPPR
{

    public class CollectibleCounter : MonoBehaviour
    {
        public TextMeshProUGUI objUI;

        void Update()
        {
            objUI.GetComponent<TextMeshProUGUI>().text = "Parts to collect: " + Collectible.objects.ToString();

            if (Collectible.objects == 0)
            {
                objUI.GetComponent<TextMeshProUGUI>().text = "Repair the machine!";
            }
        }
    }
}

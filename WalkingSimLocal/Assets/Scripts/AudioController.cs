﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    //Adds two fields to the unity inspector so that specific objects can be referenced
    [SerializeField]
    private GameObject objToDisable;
    [SerializeField]
    private GameObject objToEnable;

    private void OnTriggerEnter(Collider other)
    { 
        //Enables one object and disables another 
        objToDisable.SetActive(false);
        objToEnable.SetActive(true);
    }

}

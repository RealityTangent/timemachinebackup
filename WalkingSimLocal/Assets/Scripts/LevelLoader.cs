﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

namespace GPPR
{

    public class LevelLoader : MonoBehaviour
    {
        //animator refrence
        public Animator transition;
        //time between tranistions
        public float transitionTime = 10f;
        
        //Sets the End trugger, waits for transition time seconds then quits the game
        public void Outro()
        {
            StartCoroutine(EndGame());
        }

        IEnumerator EndGame()
        {
            transition.SetTrigger("End");

            yield return new WaitForSeconds(transitionTime);

            SceneManager.LoadScene(0, LoadSceneMode.Single);
        }
    }
}
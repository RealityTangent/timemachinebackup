using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GPPR
{

    public class TextTrigger : MonoBehaviour
    {
        //text object refrence
        public GameObject textObject;
        //timer before text expires
        public float timer;

        void Start()
        {
            //sets the text object to false until its needed
            textObject.SetActive(false);

        }

        //on collision, activates the text obj and destoys it after the timer
        void OnTriggerEnter(Collider other)
        {
            textObject.SetActive(true);
            StartCoroutine("CountDown");
        }

        //destroys the text and its objects after timer
        IEnumerator CountDown()
        {
            yield return new WaitForSeconds(timer);
            Destroy(textObject);
            Destroy(gameObject);
        }

    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GPPR
{

    public class Teleport : MonoBehaviour
    {
        //where the player will teleport to
        public Transform teleportTarget;
        //player refrence
        public CharacterController player;


        void OnTriggerEnter(Collider other)
        {
            //checks if its colliding with GO with player tag
            if (other.gameObject.CompareTag("Player"))
            {
                //gets charecter controller from player refrence
                CharacterController cc = player.GetComponent<CharacterController>();

                //disables cc since it uses transform component on update, then force sets transform, and re enables cc
                cc.enabled = false;
                player.transform.position = teleportTarget.transform.position;
                cc.enabled = true;

                Debug.Log("Teleporting");
            }

        }

    }
}
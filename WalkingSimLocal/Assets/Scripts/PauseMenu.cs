﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GPPR
{

    public class PauseMenu : MonoBehaviour
    {
        //static in case i wanna use it in other scripts but checks if the game is paused
        public static bool gameIsPaused = false;
        //the UI refrence
        public GameObject pauseMenuUI;
        //variable for consistency and time saving instead of changing every string
        private string mainMenu = "MainMenu";

        //resumes the game, turns off the UI, normalizes time, sets IsPaused to false and resets mouse state
        public void Resume()
        {
            pauseMenuUI.SetActive(false);
            Time.timeScale = 1f;
            gameIsPaused = false;
            Cursor.lockState = CursorLockMode.Locked;
        }

        //pauses the game, displays UI, stops time, sets IsPaused to true, makes the mouse free to click
        void Pause()
        {
            pauseMenuUI.SetActive(true);
            Time.timeScale = 0f;
            gameIsPaused = true;
            Cursor.lockState = CursorLockMode.Confined;
        }

        //loads the main menu scene, normalizes time, sets collectibles to 0 because every time the scene is loaded it is doubled so this is a fix
        public void LoadMenu()
        {
            Time.timeScale = 1f;
            SceneManager.LoadScene(mainMenu, LoadSceneMode.Single);
            Collectible.objects = 0;
        }

        //Quits the game
        public void QuitGame()
        {
            Application.Quit();
            Debug.Log("Quitting game");

        }


        //opens the pause menu on press of the esc key if game is not paused, resumes game on esc key if game is paused
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (gameIsPaused == true)
                {
                    Resume();
                }
                else
                {
                    Pause();
                }
            }
        }
    }
}
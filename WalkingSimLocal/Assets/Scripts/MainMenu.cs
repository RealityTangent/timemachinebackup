﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GPPR
{

    public class MainMenu : MonoBehaviour
    {
        private void Start()
        {
            //Frees the mouse so that the player can move it 
            Cursor.lockState = CursorLockMode.None;
        }

        //Loads the main game scene in single mode
        public void PlayGame()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }

        //Quits the game
        public void QuitGame()
        {
            Application.Quit();
            Debug.Log("Game is quitting");
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GPPR
{


    public class Collectible : MonoBehaviour
    {
        //Amount of collectibles in the whole scene
        public static int objects = 0;

        private void Awake()
        {
            //increments variable based on how many objects have this script
            objects++;
        }

        private void OnTriggerEnter(Collider Player)
        {
            //when the player collides removes one from variable and deactivates the GO
            objects--;

            gameObject.SetActive(false);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GPPR
{

    public class SceneChanger : MonoBehaviour
    {
        //scene player will go to
        public int destScene;

        //loads the scene by build index in variable
        void OnTriggerEnter(Collider other)
        {
            SceneManager.LoadScene(destScene);
        }

    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GPPR
{

    public class EndGame : MonoBehaviour
    {
        //level loader script refrence
        public LevelLoader levelLoader;

        //checks if the player has collected all objects then if collision plays Outro method which ends the game
        void OnTriggerEnter(Collider other)
        {
            if (Collectible.objects == 0)
            {
                levelLoader.Outro();
            }

        }

    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GPPR
{
    //self explanitory name 
    public class YouSpinMeRightRound : MonoBehaviour
    {
        // X axis rotation 
        [SerializeField]
        private int rotationSpeedX;

        // Y axis rotation
        [SerializeField]
        private int rotationSpeedY;

        // Z axis rotation
        [SerializeField]
        private int rotationSpeedZ;

        //rotates the objects depending on XYZ variables by the time speed
        void Update()
        {
            transform.Rotate(rotationSpeedX, rotationSpeedY, rotationSpeedY * Time.deltaTime);
        }
    }
}